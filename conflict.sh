#!/bin/bash

touch /home/vagrant/VCS/assignment/my_code1.sh
git add my_code1.sh
echo "echo Hello" > /home/vagrant/VCS/assignment/my_code1.sh
git commit -am 'initial'
git checkout -b new_branch1
echo "Hello world" > /home/vagrant/VCS/assignment/my_code1.sh
git commit -am 'first commit on new_branch'
git checkout master
echo "Hello World!" > /home/vagrant/VCS/assignment/my_code1.sh
git commit -am 'second commit on master'
git merge new_branch1
